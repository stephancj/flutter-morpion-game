import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Morpion Game',
      theme: ThemeData(
        useMaterial3: true,
        primarySwatch: Colors.blue,
      ),
      home: const MorpionGame(),
    );
  }
}

class MorpionGame extends StatefulWidget {
  const MorpionGame({super.key});

  @override
  _MorpionGameState createState() => _MorpionGameState();
}

class _MorpionGameState extends State<MorpionGame> {
  List<List<String>> board = List.generate(3, (_) => List.filled(3, ""));
  bool isPlayerXTurn = true;
  bool gameOver = false;

  void _handleTap(int row, int col) {
    if (!gameOver && board[row][col] == "") {
      setState(() {
        board[row][col] = isPlayerXTurn ? "X" : "O";
        isPlayerXTurn = !isPlayerXTurn;
        _checkWinner(row, col);
      });
    }
  }

  void _checkWinner(int row, int col) {
    String playerSymbol = board[row][col];

    // Check row
    if (board[row].every((symbol) => symbol == playerSymbol)) {
      gameOver = true;
      _showWinnerDialog(playerSymbol);
      return;
    }

    // Check column
    if (board.every((row) => row[col] == playerSymbol)) {
      gameOver = true;
      _showWinnerDialog(playerSymbol);
      return;
    }

    // Check diagonals
    if ((board[0][0] == playerSymbol &&
            board[1][1] == playerSymbol &&
            board[2][2] == playerSymbol) ||
        (board[0][2] == playerSymbol &&
            board[1][1] == playerSymbol &&
            board[2][0] == playerSymbol)) {
      gameOver = true;
      _showWinnerDialog(playerSymbol);
      return;
    }

    // Check for draw
    if (board.every((row) => row.every((symbol) => symbol != ""))) {
      gameOver = true;
      _showDrawDialog();
    }
  }

  void _showWinnerDialog(String playerSymbol) {
    showDialog(
      context: context,
      builder: (_) => AlertDialog(
        title: Text("Player $playerSymbol wins!"),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
              _resetBoard();
            },
            child: const Text("Play Again"),
          ),
        ],
      ),
    );
  }

  void _showDrawDialog() {
    showDialog(
      context: context,
      builder: (_) => AlertDialog(
        title: const Text("It's a draw!"),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
              _resetBoard();
            },
            child: const Text("Play Again"),
          ),
        ],
      ),
    );
  }

  void _resetBoard() {
    setState(() {
      board = List.generate(3, (_) => List.filled(3, ""));
      isPlayerXTurn = true;
      gameOver = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Morpion Game")),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            for (int row = 0; row < 3; row++)
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  for (int col = 0; col < 3; col++)
                    GestureDetector(
                      onTap: () => _handleTap(row, col),
                      child: Container(
                        width: 60,
                        height: 60,
                        decoration: BoxDecoration(
                          border: Border.all(),
                        ),
                        child: Center(
                          child: Text(
                            board[row][col],
                            style: const TextStyle(fontSize: 40),
                          ),
                        ),
                      ),
                    ),
                ],
              ),
          ],
        ),
      ),
    );
  }
}
