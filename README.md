# morpion_game

A Flutter Morpion Game Project.

The game can be used on the same application, with players taking turns on the same screen.
The first player will play with the "Xs",
the second with the "O".

As soon as a player lines up 3 symbols vertically or horizontally, no more symbols can be added, and the player who has won is announced.

The web version can be tested at:
https://stephan-flutter-morpion-game.netlify.app/#/

The apk file can be found at:
https://drive.google.com/file/d/10LFledoajvSpfJvHTdTTWCWud-8bCYhg/view?usp=drive_link